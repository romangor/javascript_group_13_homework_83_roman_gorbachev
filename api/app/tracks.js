const express = require('express');
const mongoose = require("mongoose");
const Track = require("../models/Track");
const router = express.Router();
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');


router.get('/', async (req, res, next) => {
    try {
        const tracks = await Track.find({album: req.query.album})
        res.send(tracks);
    } catch (e) {
        next(e);
    }
});

router.post('/', auth, permit('user', 'admin'), async (req, res, next) => {
    try {
        const link = req.body.youtubeLink.replace('https://www.youtube.com/watch?v=', '');
        const trackData = {
            album: req.body.album,
            title: req.body.title,
            lengthOfTrack: req.body.lengthOfTrack,
            youtubeLink: link,
            is_published: (req.user.role === 'admin')
        }
        const track = new Track(trackData);
        await track.save();
        res.send({message: 'Created new track ', track});
    } catch (e) {
        if (e instanceof mongoose.Error.ValidationError) {
            return res.status(400).send(e);
        }
        return next(e);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res, next) => {
    try {
        const track = await Track.deleteOne({_id: req.params.id});
        res.send(track);
    } catch (e) {
        next(e);
    }
});

router.post('/:id/publish', auth, permit('admin'), async (req, res, next) => {
    try {
        const track = await Track.findOneAndUpdate({_id: req.body.trackId}, {is_published: true});
        res.send(track);
    } catch (e) {
        next(e);
    }
});

module.exports = router;
