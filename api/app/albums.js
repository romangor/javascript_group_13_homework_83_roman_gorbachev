const express = require('express');
const Album = require('../models/Album');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const Artist = require("../models/Artist");
const router = express.Router();
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get("/", async (req, res, next) => {
    try {
        if (req.query.artist) {
            const albums = await Album.find({artist: req.query.artist}).populate("artist", "name information");
            return res.send(albums);
        } else {
            const albums = await Album.find().populate("artist", "name information");
            return res.send(albums);
        }
    } catch (e) {
        next(e);
    }
});

router.post("/", auth, permit('admin', 'user'), upload.single('image'), async (req, res, next) => {
    try {
        const albumData = {
            artist: req.body.artist,
            title: req.body.title,
            date: req.body.date,
            image: req.file ? req.file.filename : null,
            is_published: (req.user.role === 'admin')
        }
        const album = new Album(albumData);
        await album.save();
        return res.send(album);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const albums = await Album.find({
            artist: req.params.id
        }).populate("artist", "name information");
        return res.send(albums);
    } catch (e) {
        next(e);
    }
});

router.get('/withArtist/:id', async (req, res, next) => {
    try {
        const artist = await Artist.find({_id: req.params.id});
        const albums = await Album.find({artist: req.params.id});
        const result = {
            artist: artist,
            albums: albums
        }
        return res.send(result);
    } catch (e) {
        next(e);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res, next) => {
    try {
        const album = await Album.deleteOne({_id: req.params.id});
        res.send(album);
    } catch (e) {
        next(e);
    }
});

router.post('/:id/publish', auth, permit('admin'), async (req, res, next) => {
    try {
        const album = await Album.findOneAndUpdate({_id: req.body.albumId}, {is_published: true});
        res.send(album);
    } catch (e) {
        next(e);
    }
});

module.exports = router;