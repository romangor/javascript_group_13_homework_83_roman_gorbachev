const express = require('express');
const auth = require("../middleware/auth");
const TrackHistory = require("../models/TrackHistory");
const Album = require("../models/Album");
const Track = require("../models/Track");
const Artist = require("../models/Artist");
const router = express.Router();

router.post('/', auth, async (req, res, next) => {
    try {
        const dateTime = new Date().toString();
        const data = {
            user: req.user._id,
            track: req.body.track,
            dateTime: dateTime,
        };
        const trackHistory = new TrackHistory(data);
        await trackHistory.save();
        res.send({message: 'Added an entry to the track listening history', trackHistory});
    } catch (e) {
        next(e);
    }
});

router.get('/', auth,  async (req, res, next) => {
    try {
        const filter = {}
        const sort = {}
        sort._id = -1
        if(req.user){
           filter.user = req.user._id
        }

        const tracksHistory = await TrackHistory
            .find(filter)
            .sort(sort)
            .populate('track', 'title lengthOfTrack')
            .populate('artist', 'name');
        res.send(tracksHistory)
    } catch (e) {
        next(e);
    }
})

module.exports = router;