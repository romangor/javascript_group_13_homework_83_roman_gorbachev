const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Artist = require("../models/Artist");
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        const artists = await Artist.find();
        return res.send(artists);
    } catch (e) {
        next(e);
    }
});

router.post('/', auth, permit('user', 'admin'), upload.single('photo'), async (req, res, next) => {
    try {
        const artistData = {
            name: req.body.name,
            information: req.body.information,
            photo: req.file ? req.file.filename : null,
            is_published: (req.user.role === 'admin')
        };

        const artist = new Artist(artistData);
        await artist.save();
        return res.send({message: 'Created new Artist', id: artist._id})

    } catch (e) {
        next(e);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res, next) => {
    try {
        const artist = await Artist.deleteOne({_id: req.params.id});
        res.send(artist);
    } catch (e) {
        next(e);
    }
});

router.post('/:id/publish', auth, permit('admin'), async (req, res, next) => {
    try {
        const artist = await Artist.findOneAndUpdate({_id: req.body.artistId}, {is_published: true});
        res.send(artist);
    } catch (e) {
        next(e);
    }
});


module.exports = router;