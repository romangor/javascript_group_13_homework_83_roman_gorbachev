const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AlbumSchema = new mongoose.Schema({
    artist: {
        type: Schema.Types.ObjectId,
        ref: 'Artist'
    },
    title: String,
    date: String,
    image: String | null,
    is_published: Boolean,
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;