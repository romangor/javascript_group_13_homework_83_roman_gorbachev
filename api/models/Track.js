const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackSchema = new Schema({
    album: {
        type: Schema.Types.ObjectId,
        ref: 'Album'
    },
    title: {
        type: String,
        required: true
    },
    lengthOfTrack: {
        type: String,
        required: true
    },
    youtubeLink: {
        type: String,
    },
    is_published: Boolean,

});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;

