const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArtistSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    information: {
        type: String,
        required: true
    },
    photo: String | null,
    is_published: Boolean,
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;