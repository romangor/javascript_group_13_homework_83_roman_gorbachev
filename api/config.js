const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    mongo: {
        db: 'mongodb://localhost/musics',
        options: {useNewUrlParser: true},
    },
    facebook: {
        appId: '1338921449946802',
        appSecret: '31447e065acdd0756580fb323f40060d'
    }
};