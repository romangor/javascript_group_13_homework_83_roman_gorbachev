const mongoose = require('mongoose');
const config = require('./config');
const Artist = require("./models/Artist");
const Album = require("./models/Album");
const Track = require("./models/Track");
const TrackHistory = require("./models/TrackHistory");
const User = require("./models/User");
const {nanoid} = require("nanoid");

const run = async () => {

    await mongoose.connect(config.mongo.db, config.mongo.options);
    const collections = await mongoose.connection.db.listCollections().toArray()

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [Eminem, Rihanna] = await Artist.create({
            name: 'Eminem',
            information: 'American rapper, record producer, and actor who was known as one of the most-controversial and best-selling artists of the early 21st century.',
            photo: 'eminemPhoto.jpg',
            is_published: true,
        },
        {
            name: 'Rihanna',
            information: 'Barbadian pop and rhythm-and-blues (R&B) singer who became a worldwide star in the early 21st century',
            photo: 'rihannaPhoto.jpg',
            is_published: true,
        });

    const [The_Eminem_Show, The_Marshall_Mathers_LP, Anti] = await Album.create({
            artist: Eminem,
            title: 'The Eminem Show',
            date: '2002',
            image: 'eminem2.jpg',
            is_published: false,
        },
        {
            artist: Eminem,
            title: 'The Marshall Mathers LP',
            date: '2000',
            image: 'eminem1.jpg',
            is_published: true,
        },
        {
            artist: Rihanna,
            title: 'Anti',
            date: '2016',
            image: 'rihanna1.jpg',
            is_published: true,
        }
    );
    const [Stan, Kill_You, Solder, White_America, Till_I_Collapse, Work, Desperado] = await Track.create({
            album: The_Marshall_Mathers_LP,
            title: 'Stan',
            lengthOfTrack: '6:44',
            youtubeLink: 'gOMhN-hfMtY',
            is_published: false,
        }, {
            album: The_Marshall_Mathers_LP,
            title: 'Kill You',
            lengthOfTrack: '4:24',
            youtubeLink: 'e6abXmMqz4',
            is_published: true,
        },
        {
            album: The_Eminem_Show,
            title: 'Solder',
            lengthOfTrack: '5:24',
            youtubeLink: 'lexLAjh8fPA',
            is_published: true,
        },
        {
            album: The_Eminem_Show,
            title: 'White America',
            lengthOfTrack: '5:25',
            youtubeLink: 'RZIzD0ZfTFg',
            is_published: false,
        },
        {
            album: The_Eminem_Show,
            title: 'Till I Collapse',
            lengthOfTrack: '4:58',
            youtubeLink: 'ytQ5CYE1VZw',
            is_published: true,
        },
        {
            album: Anti,
            title: 'Work',
            lengthOfTrack: '3:39',
            youtubeLink: 'HL1UzIK-flA',
            is_published: false,
        },
        {
            album: Anti,
            title: 'Desperado',
            lengthOfTrack: '3:06',
            youtubeLink: '7awq_VEdZzk',
            is_published: true,
        }
    );
    const [Roman, Ivan] = await User.create(
        {
            email: 'roman@mail.com',
            password: '123',
            userName: 'roma',
            avatar: 'roma.jpg',
            role: 'admin',
            token: nanoid(),
            facebookID: '123'
        },
        {
            email: 'ivan@mail.com',
            password: '321',
            userName: 'Vanya',
            avatar: 'ivan.jpg',
            role: 'user',
            token: nanoid(),
            facebookID: '321'
        }
    )
    await TrackHistory.create(
        {
            user: Roman,
            track: Stan,
            dateTime: 'Wed Mar 30 2022 17:22:28',
            artist: Eminem
        },
        {
            user: Roman,
            track: Kill_You,
            dateTime: 'Wed Mar 30 2022 17:22:28',
            artist: Eminem
        },
        {
            user: Ivan,
            track: Work,
            dateTime: 'Wed Mar 30 2022 17:22:28',
            artist: Rihanna

        },
        {
            user: Ivan,
            track: Desperado,
            dateTime: 'Wed Mar 30 2022 17:22:28',
            artist: Rihanna

        },
        {
            user: Ivan,
            track: White_America,
            dateTime: 'Wed Mar 30 2022 17:22:28',
            artist: Eminem
        },
        {
            user: Roman,
            track: Solder,
            dateTime: 'Wed Mar 30 2022 17:22:28',
            artist: Eminem

        },
        {
            user: Roman,
            track: Till_I_Collapse,
            dateTime: 'Wed Mar 30 2022 17:22:28',
            artist: Eminem
        }
    )


    await mongoose.connection.close();
}
run().catch(e => console.error(e))