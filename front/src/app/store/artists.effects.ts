import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ArtistsService } from '../services/artists.service';
import {
  createArtistFailure,
  createArtistRequest,
  createArtistSuccess,
  deleteArtistFailure,
  deleteArtistRequest,
  deleteArtistSuccess, editArtistFailure,
  editArtistRequest,
  editArtistSuccess,
  fetchArtistsFailure,
  fetchArtistsRequest,
  fetchArtistsSuccess
} from './artists.actions';
import { mergeMap, map, catchError, of, tap } from 'rxjs';
import { HelpersService } from '../services/helper-service';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from './types';

@Injectable()
export class ArtistsEffects {
  fetchArtists = createEffect( () => this.actions.pipe(
    ofType(fetchArtistsRequest),
    mergeMap(() => this.artistsService.getArtists().pipe(
      map(artists => fetchArtistsSuccess({artists})),
      catchError( () => of(fetchArtistsFailure({error: 'Error'})))
    ))
  ));

  createArtist = createEffect( () => this.actions.pipe(
    ofType(createArtistRequest),
    mergeMap( ({artistData}) => this.artistsService.addArtist(artistData).pipe(
      map( () => createArtistSuccess()),
      tap( () => {
        this.helpers.openSnackbar('Artist added successfully');
        void this.router.navigate(['/']);
      }),
      catchError( () => of( createArtistFailure({error: 'Error creating artist'})))
    ))
  ));

  removeArtist = createEffect(() => this.actions.pipe(
    ofType(deleteArtistRequest),
    mergeMap(({artistId}) => this.artistsService.removeArtist(artistId).pipe(
      map( () => deleteArtistSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Artist removed');
        this.store.dispatch(fetchArtistsRequest());
      }),
      this.helpers.catchServerError(deleteArtistFailure)
    ))));

  editArtist = createEffect(() => this.actions.pipe(
    ofType(editArtistRequest),
    mergeMap(({artistId}) => this.artistsService.editArtist(artistId).pipe(
      map( () => editArtistSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Artist published successfully');
        this.store.dispatch(fetchArtistsRequest());
      }),
      this.helpers.catchServerError(editArtistFailure)
    ))));

  constructor(
    private actions: Actions,
    private artistsService: ArtistsService,
    private helpers: HelpersService,
    private router: Router,
    private store: Store<AppState>
  ){}
}

