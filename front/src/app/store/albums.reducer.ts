import {
  createAlbumFailure, createAlbumRequest,
  createAlbumSuccess,
  fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess
} from './albums.actions';
import { createReducer, on } from '@ngrx/store';
import { AlbumState } from './types';



const initialState: AlbumState = {
  albums: [],
  fetchLoadings: false,
  fetchError: null,
  createLoading: false,
  createError: null
}

export const albumsReducer = createReducer(
  initialState,
  on(fetchAlbumsRequest, state => ({...state, fetchLoadings: true})),
  on(fetchAlbumsSuccess, (state, {albums}) => ({...state, fetchLoadings: false, albums})),
  on(fetchAlbumsFailure, (state, {error}) => ({...state, fetchLoadings: false, fetchError: error})),

  on(createAlbumRequest, state => ({...state, createLoading: true})),
  on(createAlbumSuccess, state => ({...state, createLoading: false})),
  on(createAlbumFailure, (state, {error}) => ({...state, createLoading: false, createError: error,})),

);


