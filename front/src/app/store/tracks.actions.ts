import { createAction, props } from '@ngrx/store';
import { Track, TrackData, TrackError } from '../models/track.model';
import { TracksHistory } from '../models/track-history.model';

export const fetchTracksRequest = createAction('[Tracks] Fetch Request', props<{albumId: string}>());
export const fetchTracksSuccess = createAction('[Tracks] Fetch Success', props<{tracks: Track[]}>());
export const fetchTracksFailure = createAction('[Tracks] Fetch Failure', props<{error: TrackError}>());

export const addTrackRequest = createAction('[Track] Fetch Request', props<{track: string}>());
export const addTrackSuccess = createAction('[Track] Fetch Success');
export const addTrackFailure = createAction('[Track] Fetch Failure', props<{error: 'Error'}>());

export const fetchTracksHistoryRequest = createAction('[Tracks] History Request');
export const fetchTracksHistorySuccess = createAction('[Tracks] History Success', props<{tracksHistory: TracksHistory[]}>());
export const fetchTracksHistoryFailure = createAction('[Tracks] History Failure', props<{error: 'Error'}>());

export const createTrackRequest = createAction('[Track] Create Request', props<{track: TrackData}>());
export const createTrackSuccess = createAction('[Track] Create Success');
export const createTrackFailure = createAction('[Track] Create Failure', props<{error: 'Error'}>());

export const deleteTrackRequest = createAction('[Track] Delete Request', props<{trackId: string}>());
export const deleteTrackSuccess = createAction('[Track] Delete Success');
export const deleteTrackFailure = createAction('[Track] Delete Failure', props<{error: string}>());

export const editTrackRequest = createAction('[Track] Edit Request', props<{trackId: string}>());
export const editTrackSuccess = createAction('[Track] Edit Success');
export const editTrackFailure = createAction('[Track] Edit Failure', props<{error: string}>());
