import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createAlbumFailure,
  createAlbumRequest,
  createAlbumSuccess,
  deleteAlbumFailure,
  deleteAlbumRequest,
  deleteAlbumSuccess,
  editAlbumFailure,
  editAlbumRequest,
  editAlbumSuccess,
  fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess
} from './albums.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { AlbumsService } from '../services/albums.service';
import { HelpersService } from '../services/helper-service';
import { Router } from '@angular/router';


@Injectable()

export class AlbumsEffects {

  fetchAlbums = createEffect( () => this.actions.pipe(
    ofType(fetchAlbumsRequest),
    mergeMap(({artistId}) => this.albumsService.getAlbums(artistId).pipe(
      map((albums) => fetchAlbumsSuccess({albums})),
      catchError(() => of(fetchAlbumsFailure({error:'error'})))
    )
  )));

  createAlbum = createEffect( () => this.actions.pipe(
    ofType(createAlbumRequest),
    mergeMap( ({albumData}) => this.albumsService.createAlbum(albumData).pipe(
      map( () => createAlbumSuccess()),
      tap( () => {
        this.helpers.openSnackbar('Album added successfully');
        void this.router.navigate(['/']);
      }),
      catchError( () => of( createAlbumFailure({error: 'Error creating album'})))
    ))
  ));

  removeAlbum = createEffect(() => this.actions.pipe(
    ofType(deleteAlbumRequest),
    mergeMap(({albumId}) => this.albumsService.removeAlbum(albumId).pipe(
      map( () => deleteAlbumSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Album removed');
      }),
      this.helpers.catchServerError(deleteAlbumFailure)
    ))));

  editAlbum = createEffect(() => this.actions.pipe(
    ofType(editAlbumRequest),
    mergeMap(({albumId}) => this.albumsService.editAlbum(albumId).pipe(
      map( () => editAlbumSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Album published successfully');
      }),
      this.helpers.catchServerError(editAlbumFailure)
    ))));


  constructor(
    private actions: Actions,
    private albumsService: AlbumsService,
    private helpers: HelpersService,
    private router: Router,

  ){}
}
