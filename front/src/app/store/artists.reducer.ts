import { createReducer, on } from '@ngrx/store';
import {
  createArtistFailure,
  createArtistRequest,
  createArtistSuccess,
  fetchArtistsFailure,
  fetchArtistsRequest,
  fetchArtistsSuccess
} from './artists.actions';
import { ArtistState } from './types';


const initialState: ArtistState = {
  artists: [],
  fetchLoadings: false,
  fetchError: null,
  createLoading: false,
  createError: null
}


export const artistsReducer = createReducer(
  initialState,
  on(fetchArtistsRequest, state => ({...state, fetchLoadings: true})),
  on(fetchArtistsSuccess, (state, {artists}) => ({
    ...state, fetchLoadings: false, artists
  })),
  on(fetchArtistsFailure, (state, {error}) => ({
    ...state, fetchLoadings: false, fetchError: error
  })),
  on(createArtistRequest, state => ({...state, createLoading: true})),
  on(createArtistSuccess, state => ({...state, createLoading: false})),
  on(createArtistFailure, (state, {error}) => ({
  ...state,
  createLoading: false,
  createError: error,
  })),
)


