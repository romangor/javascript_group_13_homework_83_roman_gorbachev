import { createAction, props } from '@ngrx/store';
import { Artist, ArtistData } from '../models/artist.model';

export const fetchArtistsRequest = createAction('[Artists] Fetch Request');

export const fetchArtistsSuccess = createAction(
  '[Artists] Fetch Success',
        props<{artists: Artist[]}>()
);
export const fetchArtistsFailure = createAction(
  '[Artists] Fetch Failure',
  props<{error: string}>()
);
export  const createArtistRequest = createAction(
  '[Artists] Create Request',
        props<{artistData: ArtistData}>()
);
export  const createArtistSuccess = createAction(
  '[Artists] Create Success');
export  const createArtistFailure = createAction(
  '[Artists] Create Failure',
        props<{error: string}>()
);

export const deleteArtistRequest = createAction('[Artist] Delete Request', props<{artistId: string}>());
export const deleteArtistSuccess = createAction('[Artist] Delete Success');
export const deleteArtistFailure = createAction('[Artist] Delete Failure', props<{error: string}>());

export const editArtistRequest = createAction('[Artist] Edit Request', props<{artistId: string}>());
export const editArtistSuccess = createAction('[Artist] Edit Success');
export const editArtistFailure = createAction('[Artist] Edit Failure', props<{error: string}>());
