import { TracksState } from "./types";
import { createReducer, on } from '@ngrx/store';
import {
  addTrackFailure,
  addTrackRequest,
  addTrackSuccess, createTrackFailure,
  createTrackRequest, createTrackSuccess,
  fetchTracksHistoryFailure,
  fetchTracksHistoryRequest,
  fetchTracksHistorySuccess,
  fetchTracksSuccess
} from './tracks.actions';
import { fetchAlbumsFailure } from './albums.actions';

const initialState: TracksState = {
  tracks: [],
  tracksHistory: [],
  loadingTracks: false,
  trackError: null,
  trackHistoryError: ''
}

export const tracksReducer = createReducer(
  initialState,
  on(fetchTracksSuccess, state => ({...state, loadingTracks: true})),
  on(fetchTracksSuccess, (state, {tracks}) => ({...state, loadingTracks: false, tracks})),
  on(fetchAlbumsFailure, (state, {error}) => ({...state, loadingTracks: false, tracksError: error})),

  on(addTrackRequest, (state, {track}) => ({...state, loadingTracks: true, track})),
  on(addTrackSuccess, state => ({...state, loadingTracks: false})),
  on(addTrackFailure, (state, {error}) => ({...state, loadingTracks: true, trackHistoryError: error})),

  on(fetchTracksHistoryRequest, (state,) => ({...state, loadingTracks: true})),
  on(fetchTracksHistorySuccess, (state, {tracksHistory}) => ({...state, loadingTracks: false, tracksHistory})),
  on(fetchTracksHistoryFailure, (state, {error}) => ({...state, loadingTracks: true, trackHistoryError: error})),

  on(createTrackRequest, (state, {track}) => ({...state, loadingTracks: true, track})),
  on(createTrackSuccess, state => ({...state, loadingTracks: false})),
  on(createTrackFailure, (state, {error}) => ({...state, loadingTracks: true, trackHistoryError: error})),
)
