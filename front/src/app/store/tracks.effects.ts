import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap, tap } from 'rxjs';
import { TrackService } from '../services/track.services';
import {
  addTrackFailure,
  addTrackRequest,
  addTrackSuccess,
  createTrackFailure,
  createTrackRequest,
  createTrackSuccess,
  deleteTrackFailure, deleteTrackRequest,
  deleteTrackSuccess, editTrackFailure, editTrackRequest, editTrackSuccess,
  fetchTracksFailure,
  fetchTracksHistoryFailure,
  fetchTracksHistoryRequest,
  fetchTracksHistorySuccess,
  fetchTracksRequest,
  fetchTracksSuccess
} from './tracks.actions';
import { HelpersService } from '../services/helper-service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';


@Injectable()

export class TracksEffects {

  fetchTracks = createEffect(() => this.actions.pipe(
    ofType(fetchTracksRequest),
    mergeMap(({albumId}) => this.tracksService.getTracks(albumId).pipe(
      map((tracks) => fetchTracksSuccess({tracks})),
      this.helpers.catchServerError(fetchTracksFailure)
    ))));

  addTrack = createEffect(() => this.actions.pipe(
    ofType(addTrackRequest),
    mergeMap(({track}) => {
      return this.tracksService.addTrack(track).pipe(
        map(() => addTrackSuccess()),
        tap(() => this.snackbar.open('Track added successfully', 'Ok', {duration: 3000})),
        this.helpers.catchServerError(addTrackFailure)
      );
    })));

  fetchTracksHistory = createEffect(() => this.actions.pipe(
    ofType(fetchTracksHistoryRequest),
    mergeMap(() => {
      return this.tracksService.fetchTracksHistory().pipe(
        map((tracksHistory) => fetchTracksHistorySuccess({tracksHistory})),
        this.helpers.catchServerError(fetchTracksHistoryFailure)
      )
    })));

  createTrack = createEffect( () => this.actions.pipe(
    ofType(createTrackRequest),
    mergeMap( ({track}) => this.tracksService.createTrack(track).pipe(
      map( () => createTrackSuccess()),
      tap( () => {
        this.helpers.openSnackbar('Track added successfully');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(createTrackFailure)
    ))
  ));

  removeTrack = createEffect(() => this.actions.pipe(
    ofType(deleteTrackRequest),
    mergeMap(({trackId}) => this.tracksService.removeTrack(trackId).pipe(
      map( () => deleteTrackSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Track removed');
      }),
      this.helpers.catchServerError(deleteTrackFailure)
    ))));

  editTrack = createEffect(() => this.actions.pipe(
    ofType(editTrackRequest),
    mergeMap(({trackId}) => this.tracksService.editTrack(trackId).pipe(
      map( () => editTrackSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Track published successfully');
      }),
      this.helpers.catchServerError(editTrackFailure)
    ))));

  constructor(
    private actions: Actions,
    private tracksService: TrackService,
    private helpers: HelpersService,
    private snackbar: MatSnackBar,
    private router: Router
  ) {
  }
}
