import { Album } from "../models/albums.model";
import { Artist } from "../models/artist.model";
import { LoginError, RegisterError, User } from '../models/user.model';
import { Track, TrackError } from '../models/track.model';
import { TracksHistory } from '../models/track-history.model';

export type ArtistState = {
  artists: Artist[],
  fetchLoadings: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: string | null
};

export type AlbumState = {
  albums: Album[],
  fetchLoadings: boolean,
  fetchError:null | string,
  createLoading: boolean,
  createError: string | null
}

export type UsersState ={
  user: User | null,
  registerLoading: boolean,
  registerError: RegisterError | null,
  loginLoading: boolean,
  loginError: null | LoginError,
}


export type TracksState = {
  tracks: Track[],
  tracksHistory: TracksHistory[]
  loadingTracks: boolean,
  trackError:null | TrackError,
  trackHistoryError: string
}




export type AppState = {
  artists: ArtistState,
  albums: AlbumState,
  users: UsersState,
  tracks: TracksState
}

