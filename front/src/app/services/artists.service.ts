import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiArtistData, Artist, ArtistData } from '../models/artist.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArtistsService {
  constructor(private http: HttpClient) {
  }

  getArtists() {
    return this.http.get<ApiArtistData[]>(environment.apiUrl + '/artists')
      .pipe(
        map(response => {
          return response.map(artistData => {
            return new Artist(
              artistData.name,
              artistData.information,
              artistData.photo,
              artistData._id,
              artistData.is_published
            );
          });
        })
      )
  };

  addArtist(artistData: ArtistData) {
    const formData = new FormData();
    formData.append('name', artistData.name);
    formData.append('information', artistData.information);
    if (artistData.photo) {
      formData.append('photo', artistData.photo);
    }
    return this.http.post(environment.apiUrl + '/artists', formData);
  };

  removeArtist(artistId: string) {
    return this.http.delete(`${environment.apiUrl}/artists/${artistId}`);
  };

  editArtist(artistId: string) {
    return this.http.post(`${environment.apiUrl}/artists/${artistId}/publish`, {artistId: artistId});
  };
}
