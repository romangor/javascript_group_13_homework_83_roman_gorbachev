import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs';
import { Album, sendAlbumData } from '../models/albums.model';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {
  constructor(private http: HttpClient) {
  }

  getAlbums(artistId: string) {
    return this.http.get<Album[]>(environment.apiUrl + `/albums/${artistId}`)
      .pipe(
        map(response => {
          return response.map(albumData => {
            return new Album(albumData._id, albumData.title, albumData.artist, albumData.date, albumData.image, albumData.is_published);
          });
        })
      )
  };

  createAlbum(albumData: sendAlbumData) {
    const formData = new FormData();
    formData.append('title', albumData.title);
    formData.append('artist', albumData.artist);
    formData.append('date', albumData.date);
    if (albumData.image) {
      formData.append('image', albumData.image);
    }
    return this.http.post(environment.apiUrl + '/albums', formData);
  };

  removeAlbum(albumId: string) {
    return this.http.delete(`${environment.apiUrl}/albums/${albumId}`);
  }

  editAlbum(albumId: string) {
    return this.http.post(`${environment.apiUrl}/albums/${albumId}/publish`, {albumId: albumId});
  }
}
