import { HttpClient } from '@angular/common/http';
import { environment as env, environment } from 'src/environments/environment';
import { Track, TrackData } from '../models/track.model';
import { map } from 'rxjs';
import { Injectable } from '@angular/core';
import { TracksHistory } from '../models/track-history.model';


@Injectable({
  providedIn: 'root'
})
export class TrackService {
  constructor(private http: HttpClient) {
  }

  getTracks(albumId: string) {
    return this.http.get<Track[]>(`${environment.apiUrl}/tracks?album=${albumId}`)
      .pipe(map(response => {
        return response.map(track => {
          return new Track(track._id, track.album, track.title, track.lengthOfTrack, track.youtubeLink, track.is_published);
        });
      }))
  }

  fetchTracksHistory() {
    return this.http.get<TracksHistory[]>(env.apiUrl + '/historyTracks');
  };

  addTrack(trackId: string) {
    const body = {
      track: trackId
    }
    return this.http.post<{ track: string }>(env.apiUrl + '/historyTracks', body);
  };

  createTrack(track: TrackData) {
    const body = {
      album: track.album,
      title: track.title,
      lengthOfTrack: track.lengthOfTrack,
      youtubeLink: track.youtubeLink
    }
    return this.http.post<{ track: TrackData }>(env.apiUrl + '/tracks', body);
  };

  removeTrack(trackId: string) {
    return this.http.delete(`${environment.apiUrl}/tracks/${trackId}`);
  };

  editTrack(trackId: string){
    return this.http.post(`${environment.apiUrl}/tracks/${trackId}/publish`, {trackId: trackId});
  };
}
