import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginUserData, LoginUserFB, RegisterUserData, User } from '../models/user.model';
import { environment as env } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersServices {
  constructor(private http: HttpClient) {
  }

  registrationUser(user: RegisterUserData) {
    const data = new FormData();
    Object.keys(user).forEach(key => {
      if (user[key] !== null) {
        data.append(key, user[key]);
      }
    })
    return this.http.post<User>(env.apiUrl + '/users', data);
  };

  login(userData: LoginUserData) {
    return this.http.post<User>(env.apiUrl + '/users/sessions', userData);
  };

  loginWithFacebook(userFB: LoginUserFB) {
    return this.http.post<User>(env.apiUrl + '/users/facebookLogin', userFB);
  }

  logout() {
    return this.http.delete(env.apiUrl + '/users/sessions')};
}
