export class Artist {
  constructor(
    public name: string,
    public information: string,
    public photo: string,
    public _id: string,
    public is_published: boolean
  ) {
  }
}

export interface ArtistData {
  name: string,
  information: string,
  photo: File | null,
}

export interface ApiArtistData {
  _id: string,
  name: string,
  information: string,
  photo: string,
  is_published: boolean
}
