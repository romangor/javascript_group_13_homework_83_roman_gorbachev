export class TracksHistory {
  constructor(public user: string,
              public track: {
                album: string,
                title: string,
                lengthOfTrack: string,
              },
              public dateTime: string,
              public artist: {
                name: string,
                information: string,
                photo: string,
                is_published: string
              }
  ) {
  }
}
