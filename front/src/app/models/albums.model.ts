export class Album {
  constructor(
    public _id: string,
    public title: string,
    public artist: {
      _id: string
      name: string,
      information: string
    },
    public date: string,
    public image: string,
    public is_published: boolean
  ) {
  }
}

export interface AlbumData {
  title: string,
  artist: {
    name: string,
    information: string
  },
  date: string,
  image: File | null,
}

export interface sendAlbumData {
  title: string,
  artist: string,
  date: string,
  image: File | null
}
