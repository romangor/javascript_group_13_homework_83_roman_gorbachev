export class Track {
  constructor(
    public _id: string,
    public album: string,
    public title: string,
    public lengthOfTrack: string,
    public youtubeLink: string,
    public is_published: boolean
  ) {
  }
}

export interface TrackData {
  album: string,
  title: string,
  lengthOfTrack: string,
  youtubeLink: string
}

export interface TrackError {
  error: string
}


