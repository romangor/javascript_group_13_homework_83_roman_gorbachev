import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { LoginError, LoginUserData, LoginUserFB, User } from '../../models/user.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { loginUserRequest, loginUserSuccess, loginWithFacebookRequest } from '../../store/users.actions';
import { FacebookLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<null | LoginError>;
  authStateSub!: Subscription;


  constructor(private store: Store<AppState>, private authService: SocialAuthService, private http: HttpClient) {
    this.loading = store.select(state => state.users.loginLoading);
    this.error = store.select(state => state.users.loginError);
  }

  ngOnInit(){
    this.authStateSub = this.authService.authState.subscribe((user: SocialUser) => {
      const userFb: LoginUserFB = {
        authToken: user.authToken,
        id: user.id,
        email: user.email,
        userName: user.name,
        photoUrl: user.photoUrl,
      };
      this.store.dispatch(loginWithFacebookRequest({userFb}))
    });
  }

  onSubmit() {
    const userData: LoginUserData = this.form.value;
    this.store.dispatch(loginUserRequest({userData}));
  };

  fbLogin() {
    void this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  };


  ngOnDestroy(){
    this.authStateSub.unsubscribe();
  }
}
