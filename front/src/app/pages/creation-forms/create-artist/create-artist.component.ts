import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ArtistData } from 'src/app/models/artist.model';
import { AppState } from 'src/app/store/types';
import { createArtistRequest } from '../../../store/artists.actions';

@Component({
  selector: 'app-create-artist',
  templateUrl: './create-artist.component.html',
  styleUrls: ['./create-artist.component.sass']
})
export class CreateArtistComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const artistData: ArtistData = this.form.value;
    this.store.dispatch(createArtistRequest({artistData}));
  }
}
