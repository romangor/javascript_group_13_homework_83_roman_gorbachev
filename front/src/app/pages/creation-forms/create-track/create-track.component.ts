import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { Observable } from 'rxjs';
import { Artist } from '../../../models/artist.model';
import { fetchArtistsRequest } from '../../../store/artists.actions';
import { Album } from '../../../models/albums.model';
import { NgForm } from '@angular/forms';
import { fetchAlbumsRequest } from '../../../store/albums.actions';
import { TrackData } from '../../../models/track.model';
import { createTrackRequest } from '../../../store/tracks.actions';

@Component({
  selector: 'app-create-track',
  templateUrl: './create-track.component.html',
  styleUrls: ['./create-track.component.sass']
})
export class CreateTrackComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  artists: Observable<Artist[]>;
  albums: Observable<Album[]>;

  constructor(private store: Store<AppState>) {
    this.artists = store.select(state => state.artists.artists);
    this.albums = store.select(state => state.albums.albums);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchArtistsRequest());

  }

  onSubmit() {
    const track: TrackData = this.form.value;
    this.store.dispatch(createTrackRequest({track}));
  }

  onChangeArtist(artistId: string) {
    this.store.dispatch(fetchAlbumsRequest({artistId}));
  }
}
