import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { Observable } from 'rxjs';
import { Artist } from '../../../models/artist.model';
import { fetchArtistsRequest } from '../../../store/artists.actions';
import { sendAlbumData } from '../../../models/albums.model';
import { createAlbumRequest } from '../../../store/albums.actions';

@Component({
  selector: 'app-create-album',
  templateUrl: './create-album.component.html',
  styleUrls: ['./create-album.component.sass']
})
export class CreateAlbumComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  artists: Observable<Artist[]>;
  constructor(private store: Store<AppState>) {
    this.artists = store.select(state => state.artists.artists);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchArtistsRequest())
  }

  onSubmit() {
    const albumData: sendAlbumData = this.form.value;
    this.store.dispatch(createAlbumRequest({albumData}));
  }
}
