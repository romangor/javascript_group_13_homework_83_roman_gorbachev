import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Track, TrackError } from '../../models/track.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { addTrackRequest, deleteTrackRequest, editTrackRequest, fetchTracksRequest } from '../../store/tracks.actions';
import { User } from '../../models/user.model';


@Component({
  selector: 'app-tracks-list',
  templateUrl: './tracks-list.component.html',
  styleUrls: ['./tracks-list.component.sass']
})
export class TracksListComponent implements OnInit {
  tracks: Observable<Track[]>;
  loading: Observable<boolean>;
  error: Observable<null | TrackError>;
  user: Observable<null | User>;
  userData!: User;


  constructor(private store : Store<AppState>,
              private route : ActivatedRoute) {
    this.tracks = store.select(state => state.tracks.tracks);
    this.loading = store.select(state => state.tracks.loadingTracks);
    this.error = store.select(state => state.tracks.trackError);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      const albumId = params['album'];
      this.store.dispatch(fetchTracksRequest({albumId}));
    });
    this.user.subscribe( user => {
      this.userData = user!
    });
  };

  addTrack(trackId: string) {
      this.store.dispatch(addTrackRequest({track: trackId}))
  };

  remove(trackId: string) {
    this.store.dispatch(deleteTrackRequest({trackId: trackId}));
  };

  editTrack(trackId: string) {
    this.store.dispatch(editTrackRequest({trackId: trackId}));
  }
}
