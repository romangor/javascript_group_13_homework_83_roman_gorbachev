import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Artist } from '../../models/artist.model';
import { AppState } from '../../store/types';
import { Observable } from 'rxjs';
import { deleteArtistRequest, editArtistRequest, fetchArtistsRequest } from '../../store/artists.actions';



@Component({
  selector: 'app-artists-page',
  templateUrl: './artists-page.component.html',
  styleUrls: ['./artists-page.component.sass']
})
export class ArtistsPageComponent implements OnInit {

  artists: Observable<Artist[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppState>) {
    this.artists = store.select(state => state.artists.artists);
    this.loading = store.select(state => state.artists.fetchLoadings);
    this.error = store.select(state => state.artists.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchArtistsRequest());
  }


  remove(artistId: string) {
    this.store.dispatch(deleteArtistRequest({artistId: artistId}));
  }

  editArtist(artistId: string){
    this.store.dispatch(editArtistRequest({artistId: artistId}));
  }
}
