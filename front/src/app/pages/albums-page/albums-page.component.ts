import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Album } from '../../models/albums.model';
import { deleteAlbumRequest, editAlbumRequest, fetchAlbumsRequest } from '../../store/albums.actions';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-albums-page',
  templateUrl: './albums-page.component.html',
  styleUrls: ['./albums-page.component.sass']
})
export class AlbumsPageComponent implements OnInit {
  albums: Observable<Album[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;


  constructor(private store: Store<AppState>,
              private route: ActivatedRoute) {
    this.albums = store.select(state => state.albums.albums);
    this.loading = store.select(state => state.albums.fetchLoadings);
    this.error = store.select(state => state.albums.fetchError);
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      const artistId = params['artist'];
      this.store.dispatch(fetchAlbumsRequest({artistId}));
    });
  }

  remove(albumId: string) {
    this.store.dispatch(deleteAlbumRequest({albumId: albumId}));
  };

  editAlbum(albumId: string){
    this.store.dispatch(editAlbumRequest({albumId: albumId}));
  };

}
