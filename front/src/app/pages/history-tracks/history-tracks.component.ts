import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TracksHistory } from '../../models/track-history.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Router } from '@angular/router';
import { fetchTracksHistoryRequest } from '../../store/tracks.actions';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-history-tracks',
  templateUrl: './history-tracks.component.html',
  styleUrls: ['./history-tracks.component.sass']
})
export class HistoryTracksComponent implements OnInit {

  tracksHistory: Observable<TracksHistory[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  user: Observable<null | User>;
  userData!: User;

  constructor(
    private store: Store<AppState>,
    private router: Router) {
    this.tracksHistory = store.select(state => state.tracks.tracksHistory);
    this.loading = store.select(state => state.tracks.loadingTracks);
    this.error = store.select(state => state.tracks.trackHistoryError);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.user.subscribe(user => {
      if (!user) {
        void this.router.navigate(['/'])
      }
      this.userData = user!
    });
    this.store.dispatch(fetchTracksHistoryRequest());
  }
}
