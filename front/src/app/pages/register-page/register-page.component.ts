import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RegisterError } from '../../models/user.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { registerUserRequest } from '../../store/users.actions';


@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.sass']
})

export class RegisterPageComponent implements AfterViewInit, OnDestroy {
  @ViewChild('f') form!: NgForm;

  loading: Observable<boolean>;
  error: Observable<null | RegisterError>;
  errorSub!: Subscription;

  constructor(private store: Store<AppState>) {
    this.error = store.select(state => state.users.registerError);
    this.loading = store.select(state => state.users.registerLoading);
  }

  ngAfterViewInit(): void {
    this.errorSub = this.error.subscribe(error => {
      if (error) {
        const message = error.errors.email.message;
        this.form.form.get('email')?.setErrors({serverError: message});
      } else {
        this.form.form.get('email')?.setErrors({});
      }
    });
  }

  onSubmit() {
    const userData = {
      email: this.form.value.email,
      password: this.form.value.password,
      userName: this.form.value.userName,
      avatar: this.form.value.avatar
    };
    this.store.dispatch(registerUserRequest({user: userData}));
  }

  ngOnDestroy() {
    this.errorSub.unsubscribe();
  }
}
