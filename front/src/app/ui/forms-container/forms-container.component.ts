import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-forms-container',
  templateUrl: './forms-container.component.html',
  styleUrls: ['./forms-container.component.sass']
})
export class FormsContainerComponent {
    @Input() title!: string

}
