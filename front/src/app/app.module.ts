import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ArtistsPageComponent } from './pages/artists-page/artists-page.component';
import { AlbumsPageComponent } from './pages/albums-page/albums-page.component';
import { MatCardModule } from '@angular/material/card';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { artistsReducer } from './store/artists.reducer';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ArtistsEffects } from './store/artists.effects';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AlbumsEffects } from './store/albums.effects';
import { albumsReducer } from './store/albums.reducer';
import { AppRoutingModule } from './app-routing.module';
import { MatSelectModule } from '@angular/material/select';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { FlexModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { ValidateIdenticalDirective } from './directives/paswoord-validete.directive';
import { UsersEffects } from './store/users.effects';
import { usersReducer } from './store/users.reducer';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LoginComponent } from './pages/login/login.component';
import { FormsContainerComponent } from './ui/forms-container/forms-container.component';
import { MatMenuModule } from '@angular/material/menu';
import { localStorageSync } from 'ngrx-store-localstorage';
import { TracksListComponent } from './pages/tracks-list/tracks-list.component';
import { tracksReducer } from './store/tracks.reducer';
import { TracksEffects } from './store/tracks.effects';
import { HistoryTracksComponent } from './pages/history-tracks/history-tracks.component';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { CreateAlbumComponent } from './pages/creation-forms/create-album/create-album.component';
import { CreateArtistComponent } from './pages/creation-forms/create-artist/create-artist.component';
import { CreateTrackComponent } from './pages/creation-forms/create-track/create-track.component';
import { AuthInterceptor } from './auth.interceptor';
import { HasRolesDirective } from './directives/has-roles.directive';
import { UserTypeDirective } from './directives/user-type.directive';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { environment } from '../environments/environment';


const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.facebookClientId, {
        scope: 'email, public_profile',
      })
    }
  ]
};

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer];


@NgModule({
    declarations: [
        AppComponent,
        ArtistsPageComponent,
        AlbumsPageComponent,
        LayoutComponent,
        RegisterPageComponent,
        FileInputComponent,
        ValidateIdenticalDirective,
        LoginComponent,
        FormsContainerComponent,
        TracksListComponent,
        HistoryTracksComponent,
        CreateAlbumComponent,
        CreateArtistComponent,
        CreateTrackComponent,
        HasRolesDirective,
        UserTypeDirective
    ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({
      artists: artistsReducer,
      albums: albumsReducer,
      users: usersReducer,
      tracks: tracksReducer
    }, {metaReducers}),
    EffectsModule.forRoot([ArtistsEffects, AlbumsEffects, UsersEffects, TracksEffects]),
    MatProgressSpinnerModule,
    MatSelectModule,
    LayoutModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    FlexModule,
    MatInputModule,
    MatSnackBarModule,
    MatMenuModule,
    YouTubePlayerModule,
    SocialLoginModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
              {provide: 'SocialAuthServiceConfig', useValue: socialConfig }],
  bootstrap: [AppComponent]
})
export class AppModule { }
