import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumsPageComponent } from './pages/albums-page/albums-page.component';
import { ArtistsPageComponent } from './pages/artists-page/artists-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { LoginComponent } from './pages/login/login.component';
import { TracksListComponent } from './pages/tracks-list/tracks-list.component';
import { HistoryTracksComponent } from './pages/history-tracks/history-tracks.component';
import { RoleGuardService } from './services/role-guard.service';
import { CreateArtistComponent } from './pages/creation-forms/create-artist/create-artist.component';
import { CreateAlbumComponent } from './pages/creation-forms/create-album/create-album.component';
import { CreateTrackComponent } from './pages/creation-forms/create-track/create-track.component';


const routes: Routes = [
  {path: '', component: ArtistsPageComponent},
  {path: 'albums', component: AlbumsPageComponent},
  {path: 'registration', component: RegisterPageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'tracks', component: TracksListComponent},
  {path: 'historyTracks',
    component: HistoryTracksComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['admin', 'user']}
  },
  {path: 'newArtist',
    component: CreateArtistComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['admin', 'user']}
  },
  {path: 'newAlbum',
    component: CreateAlbumComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['admin', 'user']}
  },
  {path: 'newTrack',
    component: CreateTrackComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['admin', 'user']}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
